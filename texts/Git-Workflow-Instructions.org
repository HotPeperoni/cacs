# Local IspellDict: en
#+SPDX-FileCopyrightText: 2020 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+LANGUAGE: en
#+STARTUP: showeverything
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../reveal.js/css/theme/index.css" />
#+TITLE: Git Exercise
#+AUTHOR: Jens Lechtenbörger
#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil
#+DESCRIPTION: Practical exercise as open educational resource (OER) for the Feature Branch Workflow with Git

#+MACRO: gitusername lechtej
#+MACRO: gitproject cacs-2020

* Introduction

I suppose that you went through the
[[../Git-Introduction.org][Git Introduction]]
and installed and set up Git before you start here.

Please be aware that this is a new exercise, which we are going to
perform jointly for the first time, so there may be rough edges.
Please do not hesitate to ask early.  Your first steps with Git will
seem unfamiliar, but my goal is to help you in this unfamiliar
terrain.  Git is a powerful tool with many commands and options
(GitLab even more so), but the basic workflow should not be hard to
follow.  (As an aside: If you really go for Git, you probably want to
do that embedded in your daily work environment, where the most
important Git operations are available through some UI.  My daily work
environment is GNU Emacs with [[https://magit.vc/][Magit]].)

On our GitLab server, I will assign each of you as “Reporter” to my
project “{{{gitproject}}}”, and GitLab will notify you once that happened.
(This will take some time because our admins need to create your
accounts first, before I assign you manually …)
Then, you can start with this task, which is meant to practice the
[[../Git-Introduction.html#slide-git-workflow][feature
branch workflow mentioned in the Git Introduction]].

#+INCLUDE: "../programming/texts/Git-Workflow-Instructions.org" :minlevel 1

* What’s next
You have experienced Git as sample communication and collaboration
system for distributed teams.  In the
[[../Distributed-Systems-Introduction.org][next session]],
you will learn to position Git as an example of so-called
/distributed systems/, and we will revisit the notions of
[[../Distributed-Systems-Introduction.html::#slide-time-consistency][consistency and conflict]]
in that larger context.

Upcoming presentations contain “Review Questions”.
Manage your answers in additional branches (“g42-task-2”, ...) and
submit a merge request once you are done with the presentation.  Make
sure that different group members participate with commits and merge
requests.

I’ll provide feedback for merge requests.

Have fun!
