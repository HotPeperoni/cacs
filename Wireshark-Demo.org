# Local IspellDict: en
#+STARTUP: showeverything

#+SPDX-FileCopyrightText: 2020 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config.org"
#+OPTIONS: toc:nil

#+TITLE: Wireshark Demo
#+DESCRIPTION: OER demo of Wireshark as tool to understand the Internet Architecture and application behavior
#+KEYWORDS: demo, computer network, internet, web, TCP, IP, IP address, MAC address, ARP, message, datagram, frame, encapsulation, forwarding,

# Misuse subtitle for QR code:
#+SUBTITLE: [[./qr-codes/wireshark-demo-qr.png]]

* Background
[[./qr-codes/internet-qr.png]]
I suppose that you know basics about the Internet, for example,
as covered in my OER presentation
[[https://oer.gitlab.io/oer-courses/cacs/Internet.html][Introduction to the Internet]],
to which I refer subsequently.

#+INCLUDE: "distributed-systems/Internet-Wireshark.org" :minlevel 1

* Live Demo
  @@html:<video controls width="1280" height="720" data-src="./videos/wireshark-demo.mp4#t=108"></video>@@
  #+begin_notes
This video, “Wireshark Demo” by
[[https://lechten.gitlab.io/#me][Jens Lechtenbörger]],
shares the presentation’s license terms, namely
[[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]].

The demo illustrates the concept of message encapsulation in an
Internet protocol stack with the help of Wireshark.
  #+end_notes

#+INCLUDE: "license-template.org" :minlevel 2
